# reHDDPrinter #

- Generate label and print them via brother QL-570.
- Receive data from [reHDD](https://git.mosad.xyz/localhorst/reHDD) via IPC message queue.

## Example Label 
![Screenshot of label](https://git.mosad.xyz/localhorst/reHDDPrinter/raw/commit/977baf27db7d7460cac0be9eea6b545afbd118d0/output.png "Example Label ")

## Install ##

`pip install qrcode sysv-ipc pycstruct brother-ql`

```
cd /root/
git clone https://git.mosad.xyz/localhorst/reHDDPrinter.git
cd reHDDPrinter
chmod +x reHDDPrinter.py
cp reHDDPrinter.service /lib/systemd/system/reHDDPrinter.service
systemctl daemon-reload
systemctl enable --now /lib/systemd/system/reHDDPrinter.service
```

## Test printer manually ##

```
  export BROTHER_QL_PRINTER=file:///dev/usb/lp0
  export BROTHER_QL_MODEL=QL-570
```

`brother_ql print -l 62 Untitled.png`

## Printer/Paper Info ##
- Brother QL-570
- Paper With: 62mm or 696px

**Hint: Some Brother printers have enabled standby mode. This will disconnect the printer. See [Issue #12](https://git.mosad.xyz/localhorst/reHDDPrinter/issues/12) to disable standby.**

see https://github.com/pklaus/brother_ql for details for printer access



