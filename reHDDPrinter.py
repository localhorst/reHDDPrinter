#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Author:                     Hendrik Schutter, localhorst@mosad.xyz
    Date of creation:           2022/11/23
    Date of last modification:  2022/11/23
"""

import sysv_ipc 
import pycstruct
import os
import time
from brother_ql.brother_ql_create import create_label
from brother_ql.raster import BrotherQLRaster
import layouter

str_buffer_size = 64        #keep this synchronous to reHDD
msg_queue_key = 0x1B11193C0 #keep this synchronous to reHDD

file_name = "output.png"
printer_path = "/dev/usb/lp0"

def get_struct_format():
    #keep this synchronous to struct in reHDD
    driveData = pycstruct.StructDef()
    driveData.add('utf-8', 'driveIndex', length=str_buffer_size)
    driveData.add('utf-8', 'driveHours', length=str_buffer_size)
    driveData.add('utf-8', 'driveCycles', length=str_buffer_size)
    driveData.add('utf-8', 'driveErrors', length=str_buffer_size)
    driveData.add('utf-8', 'driveShredTimestamp', length=str_buffer_size)
    driveData.add('utf-8', 'driveShredDuration', length=str_buffer_size)
    driveData.add('utf-8', 'driveCapacity', length=str_buffer_size)
    driveData.add('utf-8', 'driveState', length=str_buffer_size)
    driveData.add('utf-8', 'driveModelFamiliy', length=str_buffer_size)
    driveData.add('utf-8', 'driveModelName', length=str_buffer_size)
    driveData.add('utf-8', 'driveSerialnumber', length=str_buffer_size)
    driveData.add('utf-8', 'driveReHddVersion', length=str_buffer_size)
    return driveData

def main():
    try:
        mq = sysv_ipc.MessageQueue(msg_queue_key, sysv_ipc.IPC_CREAT)

        while True:
            message, mtype = mq.receive()
            driveData = get_struct_format().deserialize(message)

            rehdd_info = layouter.ReHddInfo("https://git.mosad.xyz/localhorst/reHDD", driveData['driveReHddVersion'])
            drive = layouter.DriveData(
            drive_index=int(driveData['driveIndex']),\
            drive_state=str(driveData['driveState']),\
            modelfamiliy=str(driveData['driveModelFamiliy']),\
            modelname=str(driveData['driveModelName']),\
            capacity=int(driveData['driveCapacity']),\
            serialnumber=str(driveData['driveSerialnumber']),\
            power_on_hours=int(driveData['driveHours']),\
            power_cycle=int(driveData['driveCycles']),\
            smart_error_count=int(driveData['driveErrors']),\
            shred_timestamp=int(driveData['driveShredTimestamp']),\
            shred_duration=int(driveData['driveShredDuration']))

            while(not os.path.exists(printer_path)):
                print("Printer not found, waiting ...")
                time.sleep(30) #sleep 30

            layouter.generate_image(drive, rehdd_info, file_name)
            qlr = BrotherQLRaster("QL-570")
            create_label(qlr, file_name, '62')

            with open(printer_path, 'wb') as file:
                file.write(qlr.data)
            os.remove(file_name)
    except sysv_ipc.ExistentialError:
        print("ERROR: message queue creation failed")

if __name__ == "__main__":
    main()
